from telegram.ext import ApplicationBuilder

from app.internal.transport.bot.handlers import AllHandlers
from config.settings import TELEGRAM_BOT_TOKEN


class TelegramBot:
    """Класс Bot содержит методы для управления и настройки телеграм бота"""

    def __init__(self):
        self.application = (ApplicationBuilder()
                            .token(TELEGRAM_BOT_TOKEN)
                            .build())

    def init_handlers(self):
        """Метод для инициализации обработчиков сообщений боту"""

        for command_handler in AllHandlers.command_handlers:
            self.application.add_handler(command_handler.get_handler())

    def run(self):
        """Метод для запуска бота"""

        self.init_handlers()
        self.application.run_polling()
