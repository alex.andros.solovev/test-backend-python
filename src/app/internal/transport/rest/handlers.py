from django.http import HttpRequest, HttpResponse

from app.internal.transport.user_repository import UserRepository


async def command_me(request):
    """Обработчик endpoint-а me/

    Отображает на странице сохраненную информацию о пользователе."""

    user = await UserRepository.get_first_user()

    if user is None:
        return HttpResponse("в базе данных нет записей о пользователях")
    else:
        return HttpResponse(str(user))
