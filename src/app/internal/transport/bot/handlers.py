from app.internal.transport.bot.me_handler import MeHandler
from app.internal.transport.bot.set_phone_handler import SetPhoneHandler
from app.internal.transport.bot.start_handler import StartHandler


class AllHandlers:
    """Класс, который содержит все обработчики."""

    command_handlers = [
        StartHandler(),
        SetPhoneHandler(),
        MeHandler()
    ]
