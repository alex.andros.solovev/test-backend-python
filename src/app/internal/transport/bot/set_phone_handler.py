import re

from telegram import Update
from telegram.ext import ContextTypes

from app.internal.transport.bot.command_bot_handler import CommandBotHandler
from app.internal.transport.user_repository import UserRepository


class SetPhoneHandler(CommandBotHandler):
    """Класс обработчика команды /set_phone для телеграм бота.

    Данная команда должна найти в тексте после команды номер телефона и
    сохранить его в БД."""

    async def handle(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        user_message = ' '.join(context.args)
        phone_number = self.__format_phone_number(user_message)

        if phone_number is None:
            response_message = "phone number entered incorrectly"
        else:
            await UserRepository.save_user_phone_number(
                user_id=update.message.from_user.id,
                phone_number=phone_number,
            )
            response_message = f"accepted phone number: {phone_number}"

        await self.__send_response(
            context=context,
            chat_id=update.effective_chat.id,
            response_message=response_message,
        )

    def get_command_name(self) -> str:
        return "set_phone"

    @staticmethod
    def __format_phone_number(user_message: str):
        """Метод по заданному шаблону находит в строке первый номера телефонов,
        удаляет пробелы и тире, после чего возвращает данный номер."""

        pattern = r'(\+7|8|7).*?(\d{3}).*?(\d{3}).*?(\d{2}).*?(\d{2})'
        found_phone_number = re.search(pattern, user_message)

        if found_phone_number is None:
            return None

        return (
            found_phone_number
            .group(0)
            .replace(' ', '')
            .replace('-', '')
        )

    @staticmethod
    async def __send_response(
            context: ContextTypes.DEFAULT_TYPE, chat_id: int,
            response_message: str
    ):
        """Метод с логикой отправки сообщения в ответ на вызов команды."""

        await context.bot.send_message(chat_id=chat_id, text=response_message)
