from telegram import Update
from telegram.ext import ContextTypes

from app.internal.transport.bot.command_bot_handler import CommandBotHandler
from app.internal.transport.user_repository import UserRepository


class MeHandler(CommandBotHandler):
    """Класс обработчика команды /me для телеграм бота.

    Данная команда отображает пользователю сохраненную о нём информацию.
    Команда доступна только если пользователь предоставил номер телефона."""

    async def handle(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        user = await UserRepository.get_user(update.message.from_user.id)

        if len(user.phone_number) == 0:
            response_message = "please enter phone number"
        else:
            response_message = str(user)

        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=response_message,
        )

    def get_command_name(self) -> str:
        return "me"
