import asyncio

from telegram import Update
from telegram.ext import ContextTypes

from app.internal.transport.bot.command_bot_handler import CommandBotHandler
from app.internal.transport.user_repository import UserRepository


class StartHandler(CommandBotHandler):
    """Класс обработчика команды /start для телеграм бота.

    Данная команда должна сохранить в БД информацию о пользователе,
    а также отобразить какая именно информация была получена."""

    async def handle(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        user = update.message.from_user

        await asyncio.gather(
            asyncio.create_task(
                UserRepository.save_user(
                    user_id=user.id,
                    first_name=user.first_name,
                    last_name=user.last_name,
                    login=user.username,
                    phone_number='',
                )
            ),
            asyncio.create_task(
                self.__send_response(
                    chat_id=update.effective_chat.id,
                    context=context,
                    username=user.username
                )
            ),
        )

    def get_command_name(self) -> str:
        return "start"

    @staticmethod
    async def __send_response(
            chat_id: int, context: ContextTypes.DEFAULT_TYPE,
            username: str
    ):
        """Метод с логикой отправки сообщения в ответ на вызов команды."""

        await context.bot.send_message(
            chat_id=chat_id,
            text=f"Hello {username}!",
        )
