from abc import ABC, abstractmethod

from telegram import Update
from telegram.ext import CommandHandler, ContextTypes


class CommandBotHandler(ABC):
    """CommandBotHandler абстрактный класс,
    который должен быть унаследован каждым обработчиком команд бота.

    Данный класс декларирует методы,
    которые должен реализовывать каждый обработчик команд."""

    @abstractmethod
    async def handle(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """Метод вызывающийся для обработки сообщения пользователя."""
        pass

    @abstractmethod
    def get_command_name(self) -> str:
        """Метод возвращающий название команды
        для которой должен вызываться данный обработчик."""
        pass

    def get_handler(self) -> CommandHandler:
        """Метод возвращающий объект CommandHandler,
        который необходим для добавления обработчика."""

        return CommandHandler(self.get_command_name(), self.handle)
