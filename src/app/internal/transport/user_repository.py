import logging

from django.db import models

from app.internal.models.user import User


class UserRepository:
    """Класс содержащий методы для работы с записями в таблице БД User."""

    @staticmethod
    async def save_user(
            user_id: int,
            first_name: str,
            last_name: str,
            login: str,
            phone_number: str,
    ):
        """Асинхронный метод сохраняющий информации о пользователе."""

        user = User(
            id=user_id,
            first_name=first_name,
            last_name=last_name,
            login=login,
            phone_number=phone_number,
        )
        return await user.asave()

    @staticmethod
    async def get_user(user_id: int) -> User:
        """Асинхронный метод возвращающий информацию о пользователе
        с уникальным идентификатором user_id."""

        try:
            return await User.objects.aget(pk=user_id)
        except models.ObjectDoesNotExist as error:
            logging.error(error)

    @staticmethod
    async def get_first_user() -> User:
        """Асинхронный метод возвращающий информацию о первом пользователе."""

        try:
            return await User.objects.all().afirst()
        except models.ObjectDoesNotExist as error:
            logging.error(error)

    @staticmethod
    async def save_user_phone_number(user_id: int, phone_number: str):
        """Асинхронный метод сохраняющий номер телефона для пользователя с
        уникальным идентификатором указанным в user_id."""

        user = await UserRepository.get_user(user_id)
        user.phone_number = phone_number
        return await user.asave()
