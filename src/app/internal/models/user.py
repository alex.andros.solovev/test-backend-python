from django.db import models


class User(models.Model):
    """Класс для работы с БД таблицей пользователей.

    Attributes
    ----------

    id: int - уникальный идентификатор пользователя
    first_name: str - имя пользователя
    last_name: str - фамилия пользователя
    login: str - логин пользователя
    phone_number: str - номер телефона пользователя"""

    id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, blank=True)
    login = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=12, blank=True)

    def __str__(self):
        return (
            f"first_name: {self.first_name}\n"
            f"last_name: {self.last_name}\n"
            f"login: {self.login}\n"
            f"phone_number: {self.phone_number}"
        )

    class Meta:
        """Дополнительная информация о данной таблице

        Примечание: verbose_name и verbose_name_plural были заданы такими,
        чтобы отличать данную таблицу в админке от талицы пользователй админки

        """

        verbose_name = "Client user"
        verbose_name_plural = "Client users"
