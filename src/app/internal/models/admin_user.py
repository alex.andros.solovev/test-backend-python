from django.contrib.auth.models import AbstractUser


class AdminUser(AbstractUser):
    """Класс для работы с таблицей администраторов."""
    pass
