from django.core.management import BaseCommand

from app.internal.bot import TelegramBot


class Command(BaseCommand):
    """Класс команды для запуска телеграм бота."""

    help = "For run Telegram bot"

    def handle(self, *args, **options):
        """Метод выполняющийся при исполнении команды:
        python manage.py start_telegram_bot и запускающий бота."""

        bot = TelegramBot()
        bot.run()
