from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from app.internal.transport.rest.handlers import command_me

urlpatterns = [
    path("admin/", admin.site.urls),
    path('api/', include('app.internal.urls')),
                  path('me/', command_me),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
